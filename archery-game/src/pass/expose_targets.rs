use amethyst::{
    self,
    ecs::Read,
    renderer::{
        error::Result,
        pipe::{PipelineBuild, PipelineData, PolyPipeline},
        Encoder, Factory, Target,
    },
};
use fnv::FnvHashMap as HashMap;
use std::sync::RwLock;

pub type TargetRef = RwLock<HashMap<String, Target>>;

#[derive(Debug)]
pub struct ExposeTargetsPipeline<P>
where
    P: PolyPipeline,
{
    pipe: P,
    resize_blacklist: Option<Vec<String>>,
}

impl<P> ExposeTargetsPipeline<P>
where
    P: PolyPipeline,
{
    fn new(pipe: P, resize_blacklist: Option<Vec<String>>) -> Self {
        Self {
            pipe,
            resize_blacklist,
        }
    }

    pub fn build<B>(builder: B) -> ExposeTargetsPipelineBuilder<B>
    where
        B: PipelineBuild<Pipeline = P>,
    {
        ExposeTargetsPipelineBuilder::wrap(builder)
    }
}

impl<'a, P> PipelineData<'a> for ExposeTargetsPipeline<P>
where
    P: PolyPipeline,
{
    type Data = (Read<'a, TargetRef>, <P as PipelineData<'a>>::Data);
}

impl<P> PolyPipeline for ExposeTargetsPipeline<P>
where
    P: PolyPipeline,
{
    fn apply<'a, 'b: 'a>(
        &'a mut self,
        encoders: &mut Encoder,
        factory: Factory,
        (target_map_lock, inner_data): <Self as PipelineData<'b>>::Data,
    ) {
        {
            let mut map = target_map_lock.write().unwrap();
            *map = self.pipe.targets().clone();
        }
        self.pipe.apply(encoders, factory, inner_data);
    }

    fn new_targets(&mut self, new_targets: HashMap<String, Target>) {
        if let Some(ref blacklist) = self.resize_blacklist {
            let mut new_targets = new_targets;
            let old_targets = self.targets();
            for name in blacklist {
                if let Some(target) = old_targets.get(name) {
                    new_targets.insert(name.clone(), target.clone());
                }
            }
            self.pipe.new_targets(new_targets);
        } else {
            self.pipe.new_targets(new_targets);
        }
    }

    /// Returns an immutable reference to all targets and their name strings.
    fn targets(&self) -> &HashMap<String, Target> {
        self.pipe.targets()
    }
}

pub struct ExposeTargetsPipelineBuilder<B> {
    builder: B,
    resize_blacklist: Option<Vec<String>>,
}

impl<B> ExposeTargetsPipelineBuilder<B> {
    fn wrap(builder: B) -> Self {
        Self {
            builder,
            resize_blacklist: None,
        }
    }
    pub fn with_resize_blacklist(self, resize_blacklist: Vec<String>) -> Self {
        Self {
            resize_blacklist: Some(resize_blacklist),
            ..self
        }
    }
}

impl<B> PipelineBuild for ExposeTargetsPipelineBuilder<B>
where
    B: PipelineBuild,
{
    type Pipeline = ExposeTargetsPipeline<B::Pipeline>;
    fn build(self, fac: &mut Factory, out: &Target, multisampling: u16) -> Result<Self::Pipeline> {
        let pipe = self.builder.build(fac, out, multisampling)?;
        Ok(ExposeTargetsPipeline::new(pipe, self.resize_blacklist))
    }
}
