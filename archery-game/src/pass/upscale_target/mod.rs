//! Flat forward drawing pass that mimics a blit.

use crate::pass::expose_targets::TargetRef;
use amethyst::{
    self,
    ecs::Read,
    renderer::{
        error::Result,
        pipe::{
            pass::{Pass, PassData},
            Effect, NewEffect,
        },
        Encoder, Factory,
    },
};
pub use gfx::texture::FilterMethod;
use gfx::{
    memory::Typed,
    texture::{SamplerInfo, WrapMode},
    Factory as GfxFactory, Slice,
};

static VERT_SRC: &[u8] = include_bytes!("../shaders/vertex/upscale.glsl");
static FRAG_SRC: &[u8] = include_bytes!("../shaders/fragment/upscale.glsl");

/// Draws sprites on a 2D quad.
#[derive(Clone, Debug)]
pub struct DrawUpscaleTarget {
    target_name: String,
    filter_method: FilterMethod,
}

impl DrawUpscaleTarget
where
    Self: Pass,
{
    pub fn new<S: Into<String>>(target: S, filter_method: FilterMethod) -> Self {
        Self {
            target_name: target.into(),
            filter_method,
        }
    }
}

impl<'a> PassData<'a> for DrawUpscaleTarget {
    type Data = (Read<'a, TargetRef>,);
}

impl Pass for DrawUpscaleTarget {
    fn compile(&mut self, effect: NewEffect<'_>) -> Result<Effect> {
        effect
            .simple(VERT_SRC, FRAG_SRC)
            .without_back_face_culling()
            .with_texture("source")
            .with_output("color", None)
            .build()
    }

    fn apply<'a, 'b: 'a>(
        &'a mut self,
        encoder: &mut Encoder,
        effect: &mut Effect,
        mut factory: Factory,
        (target_ref,): <Self as PassData<'a>>::Data,
    ) {
        // get reference to render target
        // use render target "as_input" and `.raw()` method to get the view
        // push that view to effect textures
        let view = {
            let targets = target_ref.read().unwrap();
            let target = targets.get(&self.target_name).expect("Invalid target name");
            target
                .color_buf(0)
                .and_then(|b| b.clone().as_input.map(|i| i.raw().clone()))
                .unwrap()
        };

        effect.clear();

        effect.data.textures.push(view);
        effect
            .data
            .samplers
            .push(factory.create_sampler(SamplerInfo::new(self.filter_method, WrapMode::Clamp)));

        // effect
        //     .data
        //     .effect
        //     .data
        //     .textures
        //     .push(texture.view().clone());
        // effect.data.samplers.push(texture.sampler().clone());

        effect.draw(
            &Slice {
                start: 0,
                end: 6,
                base_vertex: 0,
                instances: None,
                buffer: Default::default(),
            },
            encoder,
        );
    }
}
