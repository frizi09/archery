#version 150 core

out vec2 tex_uv;

const vec2 positions[6] = vec2[](
    // First triangle
    vec2(-1.0, -1.0), // Left bottom
    vec2(1.0, -1.0), // Right bottom
    vec2(1.0, 1.0), // Right top

    // Second triangle
    vec2(1.0, 1.0), // Right top
    vec2(-1.0, 1.0), // Left top
    vec2(-1.0, -1.0)  // Left bottom
);

void main() {
    vec2 position = positions[gl_VertexID];
    tex_uv = position * 0.5 + vec2(0.5, 0.5);
    gl_Position = vec4(position, 0.0, 1.0);
}
