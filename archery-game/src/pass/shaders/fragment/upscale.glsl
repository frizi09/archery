#version 150 core

uniform sampler2D source;

in vec2 tex_uv;

out vec4 color;

void main() {
    // color = vec4(1.0, 1.0, 0.0, 1.0);
    color = texture(source, tex_uv);
}
