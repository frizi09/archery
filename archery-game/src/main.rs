mod asset_loading;
mod pass;

const PLAYFIELD_SIZE: (u32, u32) = (640, 360);

use self::pass::expose_targets::ExposeTargetsPipeline;
use crate::{
    asset_loading::{load_map, load_textures, load_tilesets, TexturesConfig},
    pass::upscale_target::{DrawUpscaleTarget, FilterMethod},
};
use amethyst::{
    core::{
        nalgebra::Orthographic3,
        transform::{Transform, TransformBundle},
    },
    ecs::Entity,
    prelude::*,
    renderer::{
        Camera, ColorMask, DepthMode, DisplayConfig, DrawFlat2D, Event, KeyboardInput, Pipeline,
        Projection, RenderBundle, Stage, Target, VirtualKeyCode, WindowEvent, ALPHA,
    },
    LogLevelFilter, LoggerConfig,
};
use archery_shared::{level::Level, tileset::TilesetMap};

#[derive(Default)]
struct GameState {
    camera_z: f32,
    camera_depth_vision: f32,
    camera: Option<Entity>,
}

impl GameState {
    fn initialise_camera(&mut self, world: &mut World) {
        // Position the camera. Here we translate it forward (out of the screen) far enough to view
        // all of the sprites. Note that camera_z is 12.0, whereas the furthest sprite is 11.0.
        //
        // For the depth, the additional + 1.0 is needed because the camera can see up to, but
        // excluding, entities with a Z coordinate that is `camera_z - camera_depth_vision`. The
        // additional distance means the camera can see up to just before -1.0 on the Z axis, so
        // we can view the sprite at 0.0.
        self.camera_z = 100.0; //self.loaded_sprite_sheet.as_ref().unwrap().sprite_count as f32;
        self.camera_depth_vision = self.camera_z + 1.0;

        self.adjust_camera(world);
    }

    fn adjust_camera(&mut self, world: &mut World) {
        if let Some(camera) = self.camera.take() {
            world
                .delete_entity(camera)
                .expect("Failed to delete camera entity.");
        }

        // let (width, height) = {
        //     let dim = world.read_resource::<ScreenDimensions>();
        //     (dim.width(), dim.height())
        // };

        let mut camera_transform = Transform::default();
        camera_transform.set_xyz(0.0, 0.0, self.camera_z);

        let camera = world
            .create_entity()
            .with(camera_transform)
            // Define the view that the camera can see. It makes sense to keep the `near` value as
            // 0.0, as this means it starts seeing anything that is 0 units in front of it. The
            // `far` value is the distance the camera can see facing the origin.
            .with(Camera::from(Projection::Orthographic(Orthographic3::new(
                0.0,
                PLAYFIELD_SIZE.0 as _,
                0.0,
                PLAYFIELD_SIZE.1 as _,
                0.0,
                self.camera_depth_vision,
            ))))
            .build();

        self.camera = Some(camera);
    }

    fn build_level(&mut self, world: &mut World) {
        load_map(world);
    }
}

impl SimpleState for GameState {
    fn on_start(&mut self, data: StateData<GameData>) {
        self.initialise_camera(data.world);

        println!("loading assets");
        load_textures(data.world);
        load_tilesets(data.world);
        println!("Starting game!");
        self.build_level(data.world);
    }

    fn handle_event(&mut self, _: StateData<GameData>, event: StateEvent) -> SimpleTrans {
        if let StateEvent::Window(event) = &event {
            match event {
                Event::WindowEvent { event, .. } => match event {
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    }
                    | WindowEvent::CloseRequested => Trans::Quit,
                    _ => Trans::None,
                },
                _ => Trans::None,
            }
        } else {
            Trans::None
        }
    }
}

fn main() -> amethyst::Result<()> {
    let mut log_config = LoggerConfig::default();
    log_config.level_filter = LogLevelFilter::Warn;
    amethyst::start_logger(log_config);

    let display_config = DisplayConfig::load("resources/display_config.ron");

    let pipe = ExposeTargetsPipeline::build(
        Pipeline::build()
            .with_target(
                Target::named("low_res")
                    .with_size((640, 360))
                    .with_num_color_bufs(1)
                    .with_depth_buf(true),
            )
            .with_target(
                Target::named("low_res_upscale")
                    .with_size((640 * 4, 360 * 4))
                    .with_num_color_bufs(1)
                    .with_depth_buf(true),
            )
            .with_stage(
                Stage::with_target("low_res")
                    .clear_target([0., 0., 0., 1.], 1.)
                    .with_pass(DrawFlat2D::new().with_transparency(
                        ColorMask::all(),
                        ALPHA,
                        Some(DepthMode::LessEqualWrite),
                    )),
            )
            .with_stage(
                Stage::with_target("low_res_upscale")
                    .with_pass(DrawUpscaleTarget::new("low_res", FilterMethod::Scale)),
            )
            .with_stage(
                Stage::with_backbuffer()
                    .clear_target([0., 0., 0., 1.], 1.)
                    .with_pass(DrawUpscaleTarget::new(
                        "low_res_upscale",
                        FilterMethod::Bilinear,
                    )),
            ),
    )
    .with_resize_blacklist(vec![
        String::from("low_res"),
        String::from("low_res_upscale"),
    ]);

    // let meta_pipe = Pipeline::build().with_stage(pipe);

    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(
            RenderBundle::new(pipe, Some(display_config.clone()))
                .with_sprite_sheet_processor()
                .with_sprite_visibility_sorting(&["transform_system"]),
        )?;

    Application::build("assets/", GameState::default())?
        .with_resource(display_config)
        .with_resource(TexturesConfig::load("resources/textures.ron"))
        .with_resource(TilesetMap::load("resources/tilesets.ron"))
        .with_resource(Level::load("resources/mapsizing.ron"))
        .build(game_data)?
        .run();

    Ok(())
}
