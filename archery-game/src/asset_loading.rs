use amethyst::{
    assets::{AssetStorage, Loader},
    core::{nalgebra::Vector3, transform::Transform},
    prelude::*,
    renderer::{
        PngFormat, Sprite, SpriteRender, SpriteSheet, SpriteSheetHandle, Texture, TextureHandle,
        TextureMetadata, Transparent,
    },
};
use archery_shared::{
    level::{ImageAtLocation, Level, TileAtLocation},
    tileset::{Tileset, TilesetMap},
};
use serde::*;
use std::collections::HashMap;

struct TilesetSpritesheetMap(HashMap<String, SpriteSheetHandle>);

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct TextureSettings {
    path: String,
}

pub type TexturesConfig = HashMap<String, TextureSettings>;

pub type TextureMap = HashMap<String, TextureHandle>;

pub fn load_textures(world: &mut World) {
    let tex_map = {
        let texture_config = world.read_resource::<TexturesConfig>();
        let loader = world.read_resource::<Loader>();
        let texture_res = world.read_resource::<AssetStorage<Texture>>();

        let mut tex_map = TextureMap::new();

        for (name, config) in &*texture_config {
            let handle = loader.load(
                config.path.to_owned(),
                PngFormat,
                TextureMetadata::srgb_scale(),
                (),
                &texture_res,
            );
            tex_map.insert(name.to_owned(), handle);
        }
        tex_map
    };

    world.add_resource(tex_map);
}

pub fn load_tilesets(world: &mut World) {
    let spritesheet_map = {
        let tilesets_config = world.read_resource::<TilesetMap>();

        let mut spritesheet_map = HashMap::new();

        for (name, settings) in &*tilesets_config {
            let sheet = load_tileset_sheet(world, &settings);
            spritesheet_map.insert(name.to_owned(), sheet);
        }
        TilesetSpritesheetMap(spritesheet_map)
    };
    world.add_resource(spritesheet_map);
}

pub fn load_map(world: &mut World) {
    // avoid reallocations
    let mut tiles_batch = Vec::with_capacity(2048);
    let mut images_batch: Vec<ImageAtLocation<_>> = Vec::with_capacity(32);
    {
        let level = &*world.read_resource::<Level>();
        let tileset_map = &*world.read_resource::<TilesetMap>();
        let spritesheet_map = &*world.read_resource::<TilesetSpritesheetMap>();
        let texture_map = &*world.read_resource::<TextureMap>();

        tiles_batch.extend(level.get_all_tiles(&spritesheet_map.0, tileset_map));
        images_batch.extend(level.get_all_images(texture_map));
    }

    for TileAtLocation {
        spritesheet_handle,
        tile_index,
        x,
        y,
        z,
    } in tiles_batch
    {
        world
            .create_entity()
            .with(Transform::from(Vector3::new(
                x as f32 * 10.0 + 5.0,
                y as f32 * 10.0 + 5.0,
                z as f32,
            )))
            .with(SpriteRender {
                sprite_sheet: spritesheet_handle,
                sprite_number: tile_index as usize,
            })
            .with(Transparent)
            .build();
    }
    for ImageAtLocation { handle, x, y, z } in images_batch {
        world
            .create_entity()
            .with(Transform::from(Vector3::new(x, y, z as f32)))
            .with(handle)
            .with(Transparent)
            .build();
    }
}

fn load_tileset_sheet(world: &World, tileset: &Tileset) -> SpriteSheetHandle {
    let texture_map = world.read_resource::<TextureMap>();
    let texture_handle = tileset.get_texture_handle(&texture_map);

    let sprites = if let Some(max_tile_id) = tileset.max_tile_id() {
        let grid_w = tileset.num_columns;
        let grid_h = tileset.num_rows;

        let mut sprites: Vec<Sprite> = Vec::with_capacity(max_tile_id as usize + 1);

        let mut sprite_id = 0;

        'outer: for y in 0..grid_h {
            for x in 0..grid_w {
                if sprite_id > max_tile_id {
                    break 'outer;
                }
                sprites.push(Sprite::from_pixel_values(
                    grid_w as u32 * 10,
                    grid_h as u32 * 10,
                    10,
                    10,
                    (x as u32) * 10,
                    (y as u32) * 10,
                    [0.0, 0.0],
                ));
                sprite_id += 1;
            }
        }
        sprites
    } else {
        vec![]
    };

    let sheet = SpriteSheet {
        texture: texture_handle.clone(),
        sprites,
    };

    let sheet_handle = {
        let loader = world.read_resource::<Loader>();
        loader.load_from_data(
            sheet,
            (),
            &world.read_resource::<AssetStorage<SpriteSheet>>(),
        )
    };

    sheet_handle
}
