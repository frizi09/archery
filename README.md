strategy for pixel-perfect rendering:

- render to low-resolution buffer matching desired pixel grid
- upscale without filtering to first integer multiple that's bigger or equal to screen size
- render that buffer to screen possibly using downsampling filtering