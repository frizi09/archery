use archery_shared::{
    bitvec::BitVec,
    level::{LayerType, Level, LevelEdgeBehaviour, LevelLayer},
};
use failure::{Error, Fail};
use itertools::Itertools;
use ron::ser::to_string_pretty;
use std::{
    collections::HashMap,
    fs::{self, File},
    path::Path,
};
use tiled::Map;

fn main() -> Result<(), exitfailure::ExitFailure> {
    println!("Hello, world!");

    let level = convert_map("assets_src/mapsizing.tmx")?;

    fs::write(
        "resources/mapsizing.ron",
        to_string_pretty(&level, Default::default())?,
    )?;

    Ok(())
}

#[derive(Debug, Fail)]
enum MapReadingError {
    #[fail(display = "Map property 'edge_behaviour' is required")]
    MissingEdgeBehaviour,
    #[fail(display = "Map property 'edge_behaviour' has invalid value '{}'", 0)]
    InvalidEdgeBehaviour(String),
    #[fail(display = "Layer property '{}' has invalid value '{}'", 0, 1)]
    InvalidLayerProp(&'static str, String),
}

fn convert_map<P: AsRef<Path>>(path: P) -> Result<Level, Error> {
    use self::MapReadingError::*;
    use tiled::PropertyValue;

    let map = open_tiled_map(path)?;
    let mut level = Level::build();

    let edge_prop = map
        .properties
        .get("edge_behaviour")
        .ok_or(MissingEdgeBehaviour)?;

    let edge = match edge_prop {
        PropertyValue::StringValue(val) => match val.as_ref() {
            "Wrap" => LevelEdgeBehaviour::Wrap,
            "Clamp" => LevelEdgeBehaviour::Clamp,
            _ => return Err(InvalidEdgeBehaviour(format!("{:?}", edge_prop)).into()),
        },
        _ => return Err(InvalidEdgeBehaviour(format!("{:?}", edge_prop)).into()),
    };

    level.with_edge_behaviour(edge);

    for (layer_num, layer_data) in map.layers.iter().enumerate() {
        let layer_type =
            layer_data
                .properties
                .get("type")
                .map_or(Ok(LayerType::default()), |ty| {
                    use self::LayerType::*;
                    match ty {
                        PropertyValue::StringValue(val) => match val.as_ref() {
                            "Decoration" => Ok(Decoration),
                            "StaticCollider" => Ok(StaticCollider),
                            "Fluid" => Ok(Fluid),
                            _ => Err(InvalidLayerProp("type", format!("{:?}", ty))),
                        },
                        _ => Err(InvalidLayerProp("type", format!("{:?}", ty))),
                    }
                })?;

        let autotile = layer_data
            .properties
            .get("autotile")
            .map_or(Ok(true), |autotile| match autotile {
                PropertyValue::BoolValue(b) => Ok(*b),
                _ => Err(InvalidLayerProp("autotile", format!("{:?}", autotile))),
            })?;

        let tiles_by_tileset = layer_data
            .tiles
            .iter()
            .enumerate()
            .flat_map(|(y, row)| {
                row.iter()
                    .enumerate()
                    .map(move |(x, tile_id)| (*tile_id, x, y))
            })
            .group_by(|data| map.get_tileset_by_gid(data.0).map(|t| t.first_gid));

        let mut tiles_by_tileset_gid: HashMap<u32, Vec<_>> = HashMap::new();

        for (first_gid, group) in &tiles_by_tileset {
            if let Some(first_gid) = first_gid {
                if let Some(vec) = tiles_by_tileset_gid.get_mut(&first_gid) {
                    vec.extend(group);
                } else {
                    let vec = group.collect::<Vec<_>>();
                    tiles_by_tileset_gid.insert(first_gid, vec);
                }
            }
        }

        for (first_gid, coord_tiles) in tiles_by_tileset_gid {
            use archery_shared::level::{DataAutotile, LayerData};
            let tileset = map
                .get_tileset_by_gid(first_gid)
                .expect("first_gid mapped without being assigned to tileset");

            if tileset.name == "archery-blueprint" {
                // TODO: handle spawners and other entities
                continue;
            }

            let data = if autotile {
                let mut mask = ((0..map.width * map.height).map(|_| false)).collect::<BitVec<_>>();

                for (_, x, y) in coord_tiles {
                    let y = map.height as usize - y - 1;
                    mask.set(y * map.width as usize + x, true);
                }

                LayerData::Autotile(DataAutotile::new(
                    tileset.name.clone(),
                    map.width as u16,
                    map.height as u16,
                    mask,
                ))
            } else {
                unimplemented!()
                // LayerData::Tiles()
            };

            let mut layer = LevelLayer::build();
            layer
                .with_layer_type(layer_type)
                .with_z_index(layer_num as _)
                .with_data(data);

            level.with_layer(layer);
        }
    }

    Ok(level.build()?)
}

fn open_tiled_map<P: AsRef<Path>>(path: P) -> Result<Map, Error> {
    use tiled::parse;
    let file = File::open(path)?;
    let parsed = parse(file)?;
    Ok(parsed)
}
