use crate::HandleMap;
use bitflags::bitflags;
use rand::{seq::SliceRandom, thread_rng};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::collections::HashMap;

pub type TilesetMap = HashMap<String, Tileset>;

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct TilesetEntry {
    mask: TristateTilesetMask,
    tiles: Vec<u16>,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Tileset {
    texture: String,
    pub num_columns: u16,
    pub num_rows: u16,
    pub entries: Vec<TilesetEntry>,
}

impl Tileset {
    pub fn get_tile_id(&self, mask: TilesetMask) -> Option<u16> {
        self.entries
            .iter()
            .find(|entry| entry.mask.test(mask))
            .and_then(|entry| entry.tiles.choose(&mut thread_rng()).cloned())
    }
    pub fn max_tile_id(&self) -> Option<u16> {
        self.entries
            .iter()
            .flat_map(|e| e.tiles.iter())
            .max()
            .cloned()
    }
    pub fn get_texture_handle<H: Clone>(&self, map: &HandleMap<H>) -> H {
        map.get(&self.texture)
            .unwrap_or_else(|| panic!("Missing tileset texture named '{}'", self.texture))
            .clone()
    }
}

bitflags! {
    #[derive(Default, Serialize, Deserialize)]
    pub struct TilesetMask: u16 {
        const TOP_LEFT = 1 << 0;
        const TOP = 1 << 1;
        const TOP_RIGHT = 1 << 2;
        const LEFT = 1 << 3;
        const CENTER = 1 << 4;
        const RIGHT = 1 << 5;
        const BOTTOM_LEFT = 1 << 6;
        const BOTTOM = 1 << 7;
        const BOTTOM_RIGHT = 1 << 8;
    }
}

impl TilesetMask {
    pub fn from_parts(parts: [bool; 9]) -> Self {
        parts
            .iter()
            .enumerate()
            .fold(TilesetMask::empty(), |acc, (i, part)| match part {
                true => acc | TilesetMask::from_bits_truncate(1 << i),
                _ => acc,
            })
    }
}

enum MaskState {
    One,
    Zero,
    DontCare,
}

#[derive(Default, Clone, Copy, Debug)]
struct TristateTilesetMask {
    check_bits: TilesetMask,
    value_bits: TilesetMask,
}

impl TristateTilesetMask {
    pub fn test(&self, mask: TilesetMask) -> bool {
        mask & self.check_bits == self.value_bits
    }

    fn bit_state_to_string(&self, bit: TilesetMask) -> &'static str {
        if !self.check_bits.contains(bit) {
            return "x";
        }
        if self.value_bits.contains(bit) {
            return "1";
        }
        return "0";
    }

    fn from_separate_states(states: [MaskState; 9]) -> Self {
        TristateTilesetMask {
            check_bits: states
                .iter()
                .enumerate()
                .fold(TilesetMask::empty(), |acc, (i, state)| match state {
                    MaskState::DontCare => acc,
                    _ => acc | TilesetMask::from_bits_truncate(1 << i),
                }),
            value_bits: states
                .iter()
                .enumerate()
                .fold(TilesetMask::empty(), |acc, (i, state)| match state {
                    MaskState::One => acc | TilesetMask::from_bits_truncate(1 << i),
                    _ => acc,
                }),
        }
    }
}

impl Serialize for TristateTilesetMask {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let serialized_string = format!(
            "{}{}{}-{}{}{}-{}{}{}",
            self.bit_state_to_string(TilesetMask::TOP_LEFT),
            self.bit_state_to_string(TilesetMask::TOP),
            self.bit_state_to_string(TilesetMask::TOP_RIGHT),
            self.bit_state_to_string(TilesetMask::LEFT),
            self.bit_state_to_string(TilesetMask::CENTER),
            self.bit_state_to_string(TilesetMask::RIGHT),
            self.bit_state_to_string(TilesetMask::BOTTOM_LEFT),
            self.bit_state_to_string(TilesetMask::BOTTOM),
            self.bit_state_to_string(TilesetMask::BOTTOM_RIGHT),
        );

        serializer.serialize_str(serialized_string.as_str())
    }
}

impl<'de> Deserialize<'de> for TristateTilesetMask {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        use serde::de::{Error, Unexpected, Visitor};
        use std::fmt::{Formatter, Result as FmtResult};

        struct TristateTilesetMaskVisitor;

        impl TristateTilesetMaskVisitor {}

        impl<'de> Visitor<'de> for TristateTilesetMaskVisitor {
            type Value = TristateTilesetMask;

            fn expecting(&self, fmt: &mut Formatter) -> FmtResult {
                fmt.write_str("a string in format \"000-111-xxx\"")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: Error,
            {
                const DELIM: u8 = '-' as u8;
                let char_to_state = |c: u8| -> Result<MaskState, E> {
                    match c as char {
                        'x' => Ok(MaskState::DontCare),
                        '0' => Ok(MaskState::Zero),
                        '1' => Ok(MaskState::One),
                        _ => Err(Error::invalid_value(Unexpected::Str(s), &self)),
                    }
                };

                if s.len() != 11 {
                    return Err(Error::invalid_value(Unexpected::Str(s), &self));
                }

                let bytes = s.as_bytes();
                if bytes[3] != DELIM || bytes[7] != DELIM {
                    return Err(Error::invalid_value(Unexpected::Str(s), &self));
                }

                let states = [
                    char_to_state(bytes[0])?,
                    char_to_state(bytes[1])?,
                    char_to_state(bytes[2])?,
                    char_to_state(bytes[4])?,
                    char_to_state(bytes[5])?,
                    char_to_state(bytes[6])?,
                    char_to_state(bytes[8])?,
                    char_to_state(bytes[9])?,
                    char_to_state(bytes[10])?,
                ];
                Ok(TristateTilesetMask::from_separate_states(states))
            }
        }

        deserializer.deserialize_str(TristateTilesetMaskVisitor)
    }
}
