use std::collections::HashMap;

pub mod level;
pub mod tileset;

pub use bitvec;

pub type HandleMap<H> = HashMap<String, H>;
