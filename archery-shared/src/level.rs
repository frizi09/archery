// pub const MAP_WIDTH: u16 = 64;
// pub const MAP_HEIGHT: u16 = 36;
// pub const MAP_TILES_COUNT: usize = MAP_WIDTH as usize * MAP_HEIGHT as usize;

use crate::{
    tileset::{Tileset, TilesetMap, TilesetMask},
    HandleMap,
};
use bitvec::*;
use failure::{Error, Fail};
use log::warn;
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub struct TileAtLocation<H> {
    pub spritesheet_handle: H,
    pub tile_index: u16,
    pub x: u16,
    pub y: u16,
    pub z: u8,
}

#[derive(Debug)]
pub struct ImageAtLocation<H> {
    pub handle: H,
    pub x: f32,
    pub y: f32,
    pub z: u8,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum LayerType {
    StaticCollider,
    Decoration,
    Fluid,
}

impl Default for LayerType {
    fn default() -> Self {
        LayerType::StaticCollider
    }
}

/**
 * Stores bitmask row by row, from bottom to top
 */
#[derive(Serialize, Deserialize, Debug)]
pub enum LayerData {
    Autotile(DataAutotile),
    Tiles(DataTiles),
    Image(DataImage),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DataTiles {
    tileset_name: String,
    columns: u16,
    rows: u16,
    tiles: Vec<Option<u16>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DataAutotile {
    tileset_name: String,
    columns: u16,
    rows: u16,
    #[serde(with = "serde_bitvec")]
    mask: BitVec<LittleEndian, u8>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DataImage {
    name: String,
    x: f32,
    y: f32,
}

mod serde_bitvec {
    use bitvec::*;
    use serde::{Deserialize, Deserializer, Serialize, Serializer};

    pub fn serialize<S>(bitvec: &BitVec<LittleEndian, u8>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        bitvec.as_ref().serialize(serializer)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<BitVec<LittleEndian, u8>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let vec = Vec::deserialize(deserializer)?;
        Ok(BitVec::from(vec))
    }
}

impl DataAutotile {
    pub fn new(
        tileset_name: String,
        columns: u16,
        rows: u16,
        mask: BitVec<LittleEndian, u8>,
    ) -> Self {
        Self {
            tileset_name,
            columns,
            rows,
            mask,
        }
    }

    #[inline(always)]
    fn query(&self, x: u16, y: u16) -> bool {
        let idx = y as usize * self.columns as usize + x as usize;
        self.mask[idx]
    }

    pub fn query_mask(&self, x: u16, y: u16, edge: LevelEdgeBehaviour) -> TilesetMask {
        let px = edge.sample(x, -1, self.columns);
        let py = edge.sample(y, -1, self.rows);
        let nx = edge.sample(x, 1, self.columns);
        let ny = edge.sample(y, 1, self.rows);

        TilesetMask::from_parts([
            self.query(px, ny),
            self.query(x, ny),
            self.query(nx, ny),
            self.query(px, y),
            self.query(x, y),
            self.query(nx, y),
            self.query(px, py),
            self.query(x, py),
            self.query(nx, py),
        ])
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LevelLayer {
    z_index: u8,
    layer_type: LayerType,
    data: LayerData,
}

impl LevelLayer {
    pub fn build() -> LevelLayerBuilder {
        LevelLayerBuilder::new()
    }

    pub fn z_index(&self) -> u8 {
        self.z_index
    }
    pub fn layer_type(&self) -> LayerType {
        self.layer_type
    }

    fn get_tileset_name(&self) -> Option<&String> {
        match &self.data {
            LayerData::Autotile(inner) => Some(&inner.tileset_name),
            LayerData::Tiles(inner) => Some(&inner.tileset_name),
            LayerData::Image(_) => None,
        }
    }

    fn get_spritesheet_handle<H: Clone>(&self, tileset_spritesheets: &HandleMap<H>) -> Option<H> {
        self.get_tileset_name().and_then(|name| {
            let sheet = tileset_spritesheets.get(name).cloned();
            if sheet.is_none() {
                warn!("Missing tileset spritesheet '{}'", name);
            }
            sheet
        })
    }

    fn get_tileset<'a>(&self, tilesets: &'a TilesetMap) -> Option<&'a Tileset> {
        self.get_tileset_name().and_then(|name| {
            let tileset = tilesets.get(name);
            if tileset.is_none() {
                warn!("Missing tileset '{}'", name);
            }
            tileset
        })
    }

    fn get_tiles<'a, 'b, H: Clone + 'static>(
        &'a self,
        tilesets: &'a TilesetMap,
        tileset_spritesheets: &'b HandleMap<H>,
        edge: LevelEdgeBehaviour,
    ) -> Option<impl Iterator<Item = TileAtLocation<H>> + 'a> {
        let z_index = self.z_index();
        match &self.data {
            LayerData::Autotile(data) => {
                let handle = self.get_spritesheet_handle(tileset_spritesheets).clone();
                let tileset = self.get_tileset(tilesets);
                combine_options(handle, tileset).map(|(handle, tileset)| {
                    EvaluatedTiles::from_autotile_data(data, tileset, handle, edge, z_index)
                        .into_iter()
                })
            }
            LayerData::Tiles(data) => self
                .get_spritesheet_handle(tileset_spritesheets)
                .clone()
                .map(|handle| EvaluatedTiles::from_tiles_data(data, handle, z_index).into_iter()),
            LayerData::Image(_) => None,
        }
    }

    pub fn get_image<H: Clone>(&self, texture_map: &HandleMap<H>) -> Option<ImageAtLocation<H>> {
        match &self.data {
            LayerData::Image(data) => texture_map.get(&data.name).map(|handle| ImageAtLocation {
                handle: handle.clone(),
                x: data.x,
                y: data.y,
                z: self.z_index,
            }),
            _ => None,
        }
    }
}

fn combine_options<A, B>(opt_a: Option<A>, opt_b: Option<B>) -> Option<(A, B)> {
    if let (Some(a), Some(b)) = (opt_a, opt_b) {
        Some((a, b))
    } else {
        None
    }
}

#[derive(Debug)]
pub struct EvaluatedTiles<H> {
    spritesheet_handle: H,
    tiles: Vec<Option<u16>>,
    columns: u16,
    z_index: u8,
}

impl<H: Clone + 'static> EvaluatedTiles<H> {
    fn from_tiles_data(data: &DataTiles, spritesheet_handle: H, z_index: u8) -> Self {
        Self {
            spritesheet_handle: spritesheet_handle,
            tiles: data.tiles.clone(),
            columns: data.columns,
            z_index,
        }
    }
    fn from_autotile_data(
        data: &DataAutotile,
        tileset: &Tileset,
        spritesheet_handle: H,
        edge: LevelEdgeBehaviour,
        z_index: u8,
    ) -> Self {
        let mut tiles = vec![None; data.columns as usize * data.rows as usize];
        let mut idx = 0;
        for y in 0..data.rows {
            for x in 0..data.columns {
                let mask = data.query_mask(x, y, edge);
                tiles[idx] = tileset.get_tile_id(mask);
                idx += 1;
            }
        }

        Self {
            spritesheet_handle,
            tiles,
            columns: data.columns,
            z_index,
        }
    }

    pub fn into_iter(self) -> impl Iterator<Item = TileAtLocation<H>> {
        let columns = self.columns;
        let handle = self.spritesheet_handle.clone();
        let z_index = self.z_index;

        self.tiles
            .into_iter()
            .enumerate()
            .filter_map(move |(i, tile)| {
                tile.map(|tile_index| TileAtLocation {
                    spritesheet_handle: handle.clone(),
                    tile_index,
                    x: (i % columns as usize) as u16,
                    y: (i / columns as usize) as u16,
                    z: z_index,
                })
            })
    }
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Level {
    edge_behaviour: LevelEdgeBehaviour,
    layers: Vec<LevelLayer>,
}

impl Level {
    pub fn build() -> LevelBuilder {
        LevelBuilder::new()
    }

    pub fn get_all_tiles<'a, H: Clone + 'static>(
        &'a self,
        tileset_spritesheets: &'a HandleMap<H>,
        tilesets: &'a TilesetMap,
    ) -> impl Iterator<Item = TileAtLocation<H>> + 'a {
        self.layers
            .iter()
            .filter_map(move |layer| {
                layer.get_tiles(tilesets, tileset_spritesheets, self.edge_behaviour)
            })
            .flat_map(|evaluated| evaluated.into_iter())
    }

    pub fn get_all_images<'a, H: Clone>(
        &'a self,
        texture_map: &'a HandleMap<H>,
    ) -> impl Iterator<Item = ImageAtLocation<H>> + 'a {
        self.layers
            .iter()
            .filter_map(move |layer| layer.get_image(texture_map))
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum LevelEdgeBehaviour {
    Wrap,
    Clamp,
}

impl Default for LevelEdgeBehaviour {
    fn default() -> Self {
        LevelEdgeBehaviour::Wrap
    }
}

impl LevelEdgeBehaviour {
    fn sample(&self, x: u16, offset: i8, bound: u16) -> u16 {
        use self::LevelEdgeBehaviour::*;
        match self {
            Wrap => ((x as i16 + offset as i16 + bound as i16) % bound as i16) as u16,
            Clamp => (x as i16 + offset as i16).max(0).min(bound as i16 - 1) as u16,
        }
    }
}

// BUILDERS

#[derive(Debug, Fail)]
pub enum LevelBuildingError {
    #[fail(display = "map property '{}' missing", 0)]
    MissingProp(&'static str),
    #[fail(display = "map layer property '{}' missing", 0)]
    MissingLayerProp(&'static str),
}

pub struct LevelBuilder {
    edge_behaviour: Option<LevelEdgeBehaviour>,
    layers: Vec<LevelLayerBuilder>,
}

impl LevelBuilder {
    fn new() -> Self {
        Self {
            edge_behaviour: None,
            layers: vec![],
        }
    }

    pub fn with_layer(&mut self, layer_builder: LevelLayerBuilder) -> &mut Self {
        self.layers.push(layer_builder);
        self
    }

    pub fn with_edge_behaviour(&mut self, edge_behaviour: LevelEdgeBehaviour) -> &mut Self {
        self.edge_behaviour = Some(edge_behaviour);
        self
    }

    pub fn build(self) -> Result<Level, LevelBuildingError> {
        use self::LevelBuildingError::MissingProp;
        Ok(Level {
            edge_behaviour: self.edge_behaviour.ok_or(MissingProp("edge_behaviour"))?,
            layers: self
                .layers
                .into_iter()
                .map(|b| b.build())
                .collect::<Result<Vec<_>, _>>()?,
        })
    }
}

pub struct LevelLayerBuilder {
    z_index: Option<u8>,
    layer_type: Option<LayerType>,
    data: Option<LayerData>,
}

impl LevelLayerBuilder {
    fn new() -> Self {
        Self {
            z_index: None,
            layer_type: None,
            data: None,
        }
    }

    pub fn build(self) -> Result<LevelLayer, LevelBuildingError> {
        use self::LevelBuildingError::MissingLayerProp;
        Ok(LevelLayer {
            z_index: self.z_index.ok_or(MissingLayerProp("z_index"))?,
            layer_type: self.layer_type.ok_or(MissingLayerProp("layer_type"))?,
            data: self.data.ok_or(MissingLayerProp("data"))?,
        })
    }

    pub fn with_z_index(&mut self, z_index: u8) -> &mut Self {
        self.z_index = Some(z_index);
        self
    }

    pub fn with_layer_type(&mut self, layer_type: LayerType) -> &mut Self {
        self.layer_type = Some(layer_type);
        self
    }

    pub fn with_data(&mut self, data: LayerData) -> &mut Self {
        self.data = Some(data);
        self
    }
}
